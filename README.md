# DDM documentation

This repository contains some basic documentation for the DDM processing Software of the University of Edinburgh's Soft Matter group.

Most of this documentation can be accessed through this repository's [Wiki](https://git.ecdf.ed.ac.uk/jarlt/ddm-documentation/-/wikis/home) page.

